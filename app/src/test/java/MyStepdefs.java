import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class MyStepdefs {
    @Given("app is opened")
    public void appIsOpened() {
    }

    @When("User taps on the statistics button")
    public void userTapsOnTheStatisticsButton() {
    }

    @Then("the statistics open")
    public void theStatisticsOpen() {
    }

    @And("he can choose between his overall statistics and the statistics of the last game")
    public void heCanChooseBetweenHisOverallStatisticsAndTheStatisticsOfTheLastGame() {
    }

    @Given("app is opened and no user data is saved")
    public void appIsOpenedAndNoUserDataIsSaved() {
    }

    @Then("a message will popup to notify the user to play to get stats")
    public void aMessageWillPopupToNotifyTheUserToPlayToGetStats() {
    }

    @Given("app is opened and Username is not empty")
    public void appIsOpenedAndUsernameIsNotEmpty() {
    }

    @When("Start a game is presses")
    public void startAGameIsPresses() {
    }

    @Then("Menu of the different game typs is opened")
    public void menuOfTheDifferentGameTypsIsOpened() {
    }

    @And("User clicks on Singleplayer")
    public void userClicksOnSingleplayer() {
    }

    @Then("Play field is shown")
    public void playFieldIsShown() {
    }

    @And("User clicks on Play")
    public void userClicksOnPlay() {
    }

    @Then("background and troopselector are available loaded")
    public void backgroundAndTroopselectorAreAvailableLoaded() {
    }

    @Then("Menu of the different game types is opened")
    public void menuOfTheDifferentGameTypesIsOpened() {
    }

    @And("User clicks on Join Game")
    public void userClicksOnJoinGame() {
    }

    @Given("theres an open Game")
    public void theresAnOpenGame() {
    }

    @Then("Table of active games is shown")
    public void tableOfActiveGamesIsShown() {
    }

    @And("User clicks on Join")
    public void userClicksOnJoin() {
    }

    @Then("connection between both will be created and the game loads")
    public void connectionBetweenBothWillBeCreatedAndTheGameLoads() {
    }
}
