package com.wordpress.dhbwar;

import org.junit.Test;

import static org.junit.Assert.*;

public class TowerTest {

    @Test
    public void switchMode() {

       Tower tower1 =  new Tower(0, 0, null, true);


       Player player1 = new Player(100,200,"Tets");
       Player player2 = new Player(100,200,"Test");

        player1.troopsOfPlayer.add(new Troop1(0, 0,null , 100, 10, true));
        player2.troopsOfPlayer.add(new Troop1(0, 0,null , 110, 10, false));
        player2.troopsOfPlayer.add(new Troop3_Giant(0, 0,null , 310, 10, false));


        tower1.attack(player2.troopsOfPlayer.get(0),player2.troopsOfPlayer,false);
        assertEquals(new Troop1(0, 0,null , 100, 10, true).health-tower1.damage,player2.troopsOfPlayer.get(0).health);
        assertEquals(new Troop3_Giant(0, 0,null , 100, 10, true).health,player2.troopsOfPlayer.get(1).health);

        tower1.switchMode();
        tower1.attack(player2.troopsOfPlayer.get(0),player2.troopsOfPlayer,false);
        assertEquals(new Troop1(0, 0,null , 100, 10, true).health-tower1.damage*2,player2.troopsOfPlayer.get(0).health);
        assertEquals(new Troop3_Giant(0, 0,null , 100, 10, true).health-tower1.damage,player2.troopsOfPlayer.get(1).health);

    }

    @Test
    public void testRange(){
        Tower tower1 =  new Tower(0, 0, null, true);
        int range = tower1.range;
        tower1.switchMode();
        assertEquals(range/2,tower1.range);
    }
}