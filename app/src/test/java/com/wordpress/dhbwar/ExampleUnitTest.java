package com.wordpress.dhbwar;

import android.content.res.Resources;
import io.cucumber.core.resource.Resource;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }


    @Test
    public void checkAttack(){

        Player player1 = new Player(100,200,"Tets");
        Player player2 = new Player(100,200,"Test");

        player1.troopsOfPlayer.add(new Troop1(1000, 1000,null , 100, 10, true));
        player2.troopsOfPlayer.add(new Troop3_Giant(1000, 1000,null , 110, 10, false));

        player1.troopsOfPlayer.get(0).attack(player2.troopsOfPlayer.get(0),player2.troopsOfPlayer,false);



        //dummy2 = (Troop1)dummy1.attack(dummy2);
        assertEquals(Troop3_Giant.maxhealth-  player1.troopsOfPlayer.get(0).damage,player2.troopsOfPlayer.get(0).health);
    }

    @Test
    public void checkforRange(){

        Player player1 = new Player(100,200,"Tets");
        Player player2 = new Player(100,200,"Test");

        player1.troopsOfPlayer.add(new Troop1(0, 0,null , 100, 10, true));
        player2.troopsOfPlayer.add(new Troop3_Giant(0, 0,null , 210, 10, false));

        player1.troopsOfPlayer.get(0).attack(player2.troopsOfPlayer.get(0),player2.troopsOfPlayer,false);


        boolean inRange = false;

        player1.troopsOfPlayer.get(0).move();

        if (player1.troopsOfPlayer.get(0).x + player1.troopsOfPlayer.get(0).range >= player2.troopsOfPlayer.get(0).x){
            inRange = true;
        }
        assertTrue(inRange);

    }
}