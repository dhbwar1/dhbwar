package com.wordpress.dhbwar;

import java.util.List;

public interface AttackBehavior {

    public boolean fight(Troops frontTroop, List<Troops> attackedTroops, boolean enemy, int x, boolean move);



}
