package com.wordpress.dhbwar;


import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;

import java.util.List;

public class Troop4_Superman extends Troops{

    static int _picturecounter = 5;
    static int maxhealth = 500;
    static int ects_value = 2000;
    int counter = 0;

    static int upgradeCost = 400;

    static int maxDamage = 8;
    static int maxDamageenemy = 8;
    static int maxhealthenemy = 500;

    Troop4_Superman (int screenX, int screenY, Resources res, int posx, int speed, boolean player) {
        super(screenX,screenY,res,posx,speed,player);
        if (player){
            this.health = maxhealth;
            this.damage = maxDamage;
        }
        else{
            this.health = maxhealthenemy;
            this.damage = maxDamageenemy;
        }

        this.range = 350;
        this.ects_cost = ects_value;
        this.attack = new SingleTargetBehavior(this.damage,this.speed,this.range);

        this.picturecounter = _picturecounter;
        this.pict= new int[]{R.drawable.superman1, R.drawable.superman2, R.drawable.superman3, R.drawable.superman4, R.drawable.superman5};
        if (res != null) {
            setPictures(res, screenX, screenY);
        }
        this.y  = (screenY-(this.picture.getHeight())) - (screenY/5);
    }

    public static void upgrade(boolean player){
        if (player){
            upgradeCost += 200;
            ects_value +=100;
            maxhealth +=25;
            maxDamage +=1;
        }
        else {
            maxhealthenemy +=25;
            maxDamageenemy +=1;
        }

    }







}
