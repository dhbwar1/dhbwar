package com.wordpress.dhbwar;


import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;

import java.util.List;

public class Troop1 extends Troops{

    static int _picturecounter = 12;
    static int maxhealth = 400;
    static int maxhealthEnemy =400;
    int counter = 0;

    static int ects_value = 100;
    static int upgradeCost = 50;



    Troop1 (int screenX, int screenY, Resources res, int posx, int speed, boolean player) {
        super(screenX,screenY,res,posx,speed,player);
        if (player){
            this.health = maxhealth;
        }
        else{
            this.health = maxhealthEnemy;
        }

        this.damage = 6;
        this.range = 210;
        this.ects_cost = ects_value;
        this.picturecounter = _picturecounter;
        this.attack = new SingleTargetBehavior(this.damage,this.speed,this.range);
        this.pict = new int[]{R.drawable.stickman1, R.drawable.stickman2, R.drawable.stickman3, R.drawable.stickman4, R.drawable.stickman5, R.drawable.stickman6, R.drawable.stickman7, R.drawable.stickman8, R.drawable.stickman9, R.drawable.stickman10, R.drawable.stickman11, R.drawable.stickman12};
        if (res != null) {
            setPictures(res, screenX, screenY);
            this.y = screenY-(this.picture.getHeight());
        }


    }

    public static void upgrade(boolean player){
        if (player){
            upgradeCost += 20;
            ects_value +=10;
            maxhealth +=10;
        }
        else{
            maxhealthEnemy+=10;
        }

    }









}
