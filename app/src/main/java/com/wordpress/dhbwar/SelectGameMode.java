package com.wordpress.dhbwar;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class SelectGameMode extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_game_mode);
        Intent intent = getIntent();
        String Username = intent.getStringExtra(StartScreen.EXTRA_TEXT);

        TextView Usernamegame;

        Usernamegame = (TextView)findViewById(R.id.Usertext);
        Usernamegame.setText(Username);
        Button GoBackBtn = (Button)findViewById(R.id.button7);
        Button StartGameBtn = findViewById(R.id.button5);

        GoBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               openactivity1();
            }
        });

        StartGameBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openactivitygame();
            }
        });
    }



    public void openactivity1(){
        Intent intent = new Intent(this, StartScreen.class);
        startActivity(intent);
    }

    public void openactivitygame(){
        Intent intent = new Intent(this, GameActivity.class);
        startActivity(intent);
    }

}