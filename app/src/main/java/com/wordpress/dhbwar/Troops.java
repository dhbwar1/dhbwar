package com.wordpress.dhbwar;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;

import java.util.List;
import java.util.Vector;

public abstract class Troops {

    boolean attacking = false;
    int speed;
    int damage;
    int health;
    List<Bitmap > pictu = new Vector();
    int picturecounter;
    int counter = 0;
    int x = 0, y;
    Bitmap picture;
    int range;
    int ects_cost;
    boolean player;
    boolean updatetrooppicture;
    boolean alive;
    boolean update;
    boolean move;
    public void move(){
        this.x += speed;
    }
    public AttackBehavior attack;
    public static int ects_value;
    public int pict[];



    public void attack(Troops frontTroop, List<Troops> attackedTroops, boolean enemy){
        move = attack.fight(frontTroop,attackedTroops,enemy,x,move);
        attacking = !move;
    }

    Troops (int screenX, int screenY, Resources res, int posx, int speed, boolean player){
        this.speed = speed;
        this.updatetrooppicture = true;
        this.alive = true;
        this.player = player;
        this.x = posx;
        this.update = false;


    }
    public void setPictures(Resources res,int  screenX, int screenY){
        int divider = 8;
        System.out.println(this.getClass().getName());
        if(this.getClass().getName() == Troop3_Giant.class.getName()){
            divider = 4;
        }
            for(int i = 0; i < picturecounter; i++){
                this.picture = BitmapFactory.decodeResource(res, pict[i]);
                this.picture = Bitmap.createScaledBitmap(this.picture,screenX/divider,screenY/divider,true);
                if (!this.player) {
                    Bitmap bInput, bOutput;
                    Matrix matrix = new Matrix();
                    matrix.preScale(-1.0f, 1.0f);
                    this.picture = Bitmap.createBitmap(this.picture, 0, 0, this.picture.getWidth(), picture.getHeight(), matrix, true);
                }
                this.pictu.add(this.picture);
            }

    }
}


