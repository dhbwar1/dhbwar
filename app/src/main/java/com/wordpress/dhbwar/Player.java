package com.wordpress.dhbwar;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.widget.TextView;
import android.graphics.*;
import java.util.List;
import java.util.Vector;

public class Player  {

    int ECTs;
    int Healthpoints;
    String name;
    public Boolean[] troops = new Boolean[8];
    public List<Troops> troopsOfPlayer = new Vector();
    Troops fastesttroopPos = null;

    TextView ECTSandHealth;
    public Player(int ECTs,int healthpoints,String name) {
        this.ECTs = ECTs;
        this.Healthpoints = healthpoints;
        this.name = name;
    }

    public void drawTroops(Canvas canvas, Paint paint1, Paint paint2){
        for (int i = 0;i< troopsOfPlayer.size();i++){
            try {

                canvas.drawBitmap(troopsOfPlayer.get(i).picture, troopsOfPlayer.get(i).x,troopsOfPlayer.get(i).y, paint1);
                canvas.drawText("HP:" + troopsOfPlayer.get(i).health, troopsOfPlayer.get(i).x + 40,troopsOfPlayer.get(i).y - 140 - (i*20), paint2);
            }
            catch (Exception e){
                System.out.println("What:" + e.getMessage());
            }

        }

    }
    public void buyTroop(String nameTroop, int ects_cost, GameView gameView){
        if (ECTs - ects_cost >= 0 ) {
            ECTs -= ects_cost;
            switch (nameTroop){
                case "Troop1":
                    troopsOfPlayer.add(new Troop1(gameView.screenX, gameView.screenY, gameView.conti.getResources(), gameView.background1.x, 6, true));
                    break;
                case "Troop2_Wizard":
                    gameView.player.troopsOfPlayer.add(new Troop2_Wizard(gameView.screenX, gameView.screenY, gameView.conti.getResources(), gameView.background1.x, 6, true));
                    break;
                case "Troop3_Giant":
                    gameView.player.troopsOfPlayer.add(new Troop3_Giant(gameView.screenX, gameView.screenY, gameView.conti.getResources(), gameView.background1.x, 3, true));
                    break;
                case "Troop4_Superman":
                    gameView.player.troopsOfPlayer.add(new Troop4_Superman(gameView.screenX, gameView.screenY, gameView.conti.getResources(), gameView.background1.x, 10, true));
                    break;
                default:
                    break;
            }

        }


    }
    public void upgradeTroop(String nameTroop, int upgradeCost){
        if (ECTs - upgradeCost >= 0) {
            ECTs -= upgradeCost;
            switch (nameTroop){
                case "Troop1":
                    Troop1.upgrade(true);
                    break;
                case "Troop2_Wizard":
                    Troop2_Wizard.upgrade(true);
                    break;
                case "Troop3_Giant":
                    Troop3_Giant.upgrade(true);
                    break;
                case "Troop4_Superman":
                    Troop4_Superman.upgrade(true);
                    break;
                default:
                    break;
            }


        }

    }


}
