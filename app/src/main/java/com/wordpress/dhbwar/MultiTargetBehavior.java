package com.wordpress.dhbwar;

import java.util.List;

public class MultiTargetBehavior implements AttackBehavior {

    int damage;
    int speed;
    int range;

    public MultiTargetBehavior(int dmg, int spd, int rg) {
        damage = dmg;
        speed = spd;
        range = rg;

    }


    public boolean fight(Troops frontTroop, List<Troops> attackedTroops, boolean enemy, int x, boolean move) {
        boolean notattacking = true;

        if (enemy) {
            for (Troops troopj : attackedTroops) {


                if (x + range >= troopj.x - 30) {
                    doDamage(troopj);
                    notattacking = false;
                }

            }
            if (x + range >= frontTroop.x-30 && frontTroop.getClass() == Tower.class){
                doDamage(frontTroop);
                notattacking = false;
            }
        } else {
            for (Troops troopj : attackedTroops) {

                if (x - range <= troopj.x + 30) {
                    doDamage(troopj);
                    notattacking = false;
                }
            }
            if (x - range <= frontTroop.x+30 && frontTroop.getClass() == Tower.class){
                doDamage(frontTroop);
                notattacking = false;
            }
        }
        return notattacking;


    }
    public void doDamage(Troops attackedTroop){
        attackedTroop.health -= damage;
        System.out.println("Friendly wiz ist attacking");
        if (attackedTroop.health <= 0) {
            attackedTroop.alive = false;
        }
    }
}
