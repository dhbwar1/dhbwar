package com.wordpress.dhbwar;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.*;

import java.util.List;
import java.util.Vector;

public class Tower extends Troops {

    int atkspeed;
    static int maxhealth = 6000;
    Bitmap canon;
    int picturecounter;
    boolean multimode = false;
    static int _picturecounter = 4;
    static int upgradeCost = 50;




    Tower(int screenX, int screenY, Resources res, boolean player) {
        super(screenX,screenY,res,0,0,player);
        this.health = maxhealth;
        this.damage = 1;
        this.range = 800;
        this.atkspeed = 1;
        this.y = 530;
        this.picturecounter = _picturecounter;
        this.attack = new SingleTargetBehavior(this.damage,0,this.range);
        if (res != null) {
            setPictures(res, screenX, screenY);
        }

    }

    public void switchMode(){
        if (this.attack.getClass() == SingleTargetBehavior.class){
            this.range = this.range/2;
            this.attack = new MultiTargetBehavior(this.damage,0,this.range);
            this.multimode = true;
        }
        else {
            this.range = this.range*2;
            this.attack = new SingleTargetBehavior(this.damage,0,this.range);
            this.multimode = false;
        }
    }
    public void setPictures(Resources res, int screenX, int screenY) {


        int[] pict = new int[_picturecounter];
        pict[0] = R.drawable.tower;
        pict[1] = R.drawable.towerdamage1;
        pict[2] = R.drawable.towerdamage2;
        pict[3] = R.drawable.towerdamage3;
        this.canon = BitmapFactory.decodeResource(res, R.drawable.canone5g);

        this.canon = Bitmap.createScaledBitmap(this.canon, screenX/20, screenY/10, true);
        if(this.player){

            Matrix matrix = new Matrix();
            matrix.preScale(-1.0f, 1.0f);
            this.canon = Bitmap.createBitmap(this.canon, 0, 0, this.canon.getWidth(), canon.getHeight(), matrix, true);
            this.range += screenX/8;
        }


        for (int i = 0; i < _picturecounter; i++) {
            this.picture = BitmapFactory.decodeResource(res, pict[i]);
            this.picture = Bitmap.createScaledBitmap(picture, screenX / 4, screenY / 2, true);
            pictu.add(picture);
        }

    }
    public void upgrade(){
        upgradeCost+= 150;
        this.health += 200;
        this.damage +=1;
    }

    public void drawAttack(Canvas canvas, Paint paint1, Troops fronttroop,  List<Troops> attackedTroops){
        if (this.attacking){
            attacking = false;
            if (player){
                paint1.setColor(Color.GREEN);
                if (this.multimode) {
                    for (int i=0;i<attackedTroops.size();i++){
                        if (this.x + this.range >=attackedTroops.get(i).x-20){
                            canvas.drawLine(this.x + this.picture.getWidth() / 2 + 60, this.y - 70, attackedTroops.get(i).x + attackedTroops.get(i).picture.getWidth() / 2, attackedTroops.get(i).y, paint1);

                        }
                    }

                } else {
                    canvas.drawLine(this.x + this.picture.getWidth() / 2 + 60, this.y - 70, fronttroop.x + fronttroop.picture.getWidth() / 2, fronttroop.y, paint1);
                }
            }
            else{
                if (fronttroop != null) {
                    paint1.setColor(Color.RED);
                    canvas.drawLine(this.x + this.picture.getWidth() / 2-60, this.y-70, fronttroop.x + fronttroop.picture.getWidth() / 2, fronttroop.y,paint1);
                }
            }
        }

    }

    public void drawBuilding(Canvas canvas, Paint paint){
        if (player){
            canvas.drawBitmap(this.picture, this.x, this.y, paint);
            canvas.drawBitmap(this.canon,this.x+this.picture.getWidth()/2, this.y-110, paint);
        }
        else{
            canvas.drawBitmap(this.picture, this.x, this.y, paint);
            canvas.drawBitmap(this.canon, (this.x+this.picture.getWidth()/2)-this.canon.getWidth(), this.y-110, paint);
        }

    }



    public void updatePicture(){

        if(this.health <= 0){
            this.picture = pictu.get(3);
        }
        else if (this.health <= (int)(this.maxhealth*0.2)){
            this.picture = pictu.get(2);
        }
        else if (this.health < (int)(this.maxhealth*0.5)){

            this.picture = pictu.get(1);
        }
        else {
            this.picture = pictu.get(0);
        }
    }

}
