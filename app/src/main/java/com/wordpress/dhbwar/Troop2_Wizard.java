package com.wordpress.dhbwar;


import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import java.util.List;

public class Troop2_Wizard extends Troops{


    static int _picturecounter = 12;
    static int maxhealth = 150;
    int counter = 0;

    static int ects_value = 350;
    static int upgradeCost = 200;
    static int maxRange = 560;
    static int maxhealtenemy = 150;
    static int maxRangeenemy = 560;


    Troop2_Wizard (int screenX, int screenY, Resources res, int posx, int speed, boolean player) {
        super(screenX,screenY,res,posx,speed,player);
        if (player){
            this.health = maxhealth;
            this.range = maxRange;
        }
        else{
            this.health = maxhealtenemy;
            this.range = maxRangeenemy;
        }

        this.damage = 2;

        this.ects_cost = ects_value;
        this.picturecounter  = _picturecounter;
        this.attack = new MultiTargetBehavior(this.damage,this.speed,this.range);
        this.pict = new int[]{R.drawable.wizard1, R.drawable.wizard2, R.drawable.wizard3, R.drawable.wizard4, R.drawable.wizard5, R.drawable.wizard6, R.drawable.wizard7, R.drawable.wizard8, R.drawable.wizard9, R.drawable.wizard10, R.drawable.wizard11, R.drawable.wizard12};
        if (res != null) {
            setPictures(res, screenX, screenY);
        }

        this.y = screenY-(this.picture.getHeight());


    }
    public static void upgrade(boolean player){
        if (player){
            upgradeCost += 20;
            ects_value +=10;
            maxhealth +=5;
            maxRange += 5;
        }
        else{
            maxhealtenemy +=5;
            maxRange +=5;
        }

    }







}
