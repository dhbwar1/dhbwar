package com.wordpress.dhbwar;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.wordpress.dhbwar.R;

public class Background {
    int x = 0, y = 0;

    Bitmap background;

    Background (int screenX, int screenY, Resources res) {


        background = BitmapFactory.decodeResource(res, R.drawable.background_big);
        if (background == null){

        }
        background = Bitmap.createScaledBitmap(background,screenX * 3,screenY,true);
    }


}
