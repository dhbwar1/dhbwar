package com.wordpress.dhbwar;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.*;
import android.view.SurfaceView;
import android.webkit.WebSettings;
import android.widget.Button;
import android.widget.TextView;

import java.time.format.TextStyle;
import java.util.Random;

public class GameView extends SurfaceView implements Runnable {

    public boolean clicked = true;
    private Thread thread;
    private boolean isPlaying = false;
    public int screenX;
    public int screenY;
    public Context conti;
    private float screenRatioX,screenRatioY;
    private final Random rand  = new Random();
    private Paint paint;
    private Troop1 Hermann;
    public final Tower playerTower;
    private final Tower enemyTower;
    public  Background background1, background2;
    public float difference = 0;
    public boolean moveLeft = false;
    public boolean moveRight = false;
    public int kills;

    public Player player;
    public Player enemy;
    public TextView mana;



    public int troopcounter = 0;
    public int recalccounter = 0;
    public Troopselector troopselector;
    public static SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;


    int rando;
    public GameView(Context context, int screenX, int screenY) {
        super(context);
        this.screenX = screenX;
        this.screenY = screenY;
        player = new Player(500,100,"Röthig");
        enemy = new Player(100,100,"der Erzfeind");
        background1 = new Background(screenX,screenY, context.getResources());
        troopselector = new Troopselector(screenX,screenY,context.getResources());
        playerTower = new Tower(screenX, screenY, context.getResources(), true);
        enemyTower = new Tower(screenX, screenY, context.getResources(), false);
        enemyTower.x = screenX*3 - (int)(enemyTower.picture.getWidth());





        paint = new Paint();

        Button test = new Button(context);
        test.setWidth(40);
        test.setHeight(40);
        test.setX(100);
        test.setY(100);
        test.setText("Meddl leude");

        conti = context;

    }



    @Override
    public void run() {

        while(isPlaying){
            rando = rand.nextInt(5000);
           if (rando < 40 && rando > 20){
                enemy.troopsOfPlayer.add(new Troop1(screenX, screenY, conti.getResources(), background1.x + background1.background.getWidth() - enemyTower.picture.getWidth()/2, -6, false));

           }
            if (rando <  20 && rando > 10){
                enemy.troopsOfPlayer.add(new Troop2_Wizard(screenX, screenY, conti.getResources(), background1.x + background1.background.getWidth() - enemyTower.picture.getWidth()/2, -6, false));
                if (rando < 15){
                    enemyTower.upgrade();
                }
            }
            if (rando < 10 && rando >5){
                enemy.troopsOfPlayer.add(new Troop3_Giant(screenX, screenY, conti.getResources(), background1.x + background1.background.getWidth() - enemyTower.picture.getWidth()/2, -3, false));

            }
            if (rando <= 3 ){
                enemy.troopsOfPlayer.add(new Troop4_Superman(screenX, screenY, conti.getResources(), background1.x + background1.background.getWidth() - enemyTower.picture.getWidth()/2, -10, false));
                switch (rando) {
                    case 0: Troop1.upgrade(false);
                        break;
                    case 1: Troop2_Wizard.upgrade(false);
                        break;
                    case 2: Troop3_Giant.upgrade(false);
                        break;
                    case 3: Troop4_Superman.upgrade(false);
                        break;
                }
            }
            recalccounter++;
            if (recalccounter > 4){
                getFrontPosition(player,false);
                getFrontPosition(enemy,true);
                recalccounter = 0;
            }
            player.ECTs += 2;

            update();
            draw();
            sleep();
            if(playerTower.alive == false || enemyTower.alive == false){
                isPlaying = false;
                drawFinishScreen();

            }
        }
    }

    public void resume(){
        isPlaying = true;
        thread = new Thread(this);
        thread.start();

    }

    public void pause(){

        try {
           isPlaying = false;
            thread.join();
        }
        catch (InterruptedException e){

        }

    }

    private void update(){
        //System.out.println("EnemyC:" + enemy.troopsOfPlayer.size());
        //System.out.println("PlayerC:" + player.troopsOfPlayer.size());
        System.out.println(enemyTower.x);
        boolean movable = true;
        System.out.println(background1.x);
        if(moveRight && background1.x >= screenX*(-1.9)){
            background1.x -= 50;
            playerTower.x -= 50;
            enemyTower.x -= 50;
            System.out.println((background1.x));
        }
        else if(moveLeft && background1.x <= -50){
            background1.x += 50;
            playerTower.x += 50;
            enemyTower.x += 50;
            System.out.println((background1.x));
        }
        if(background1.x <= screenX*(-1.9)){
            background1.x = (int)(screenX*(-1.9));
            playerTower.x = (int)(screenX*(-1.9));
            enemyTower.x = (int)(screenX - (enemyTower.picture.getWidth()-(0.07*screenX)));
            //System.out.println("dwadadawdwadjwaawdjwad" + enemyTower.x);
            movable = false;
            System.out.println((background1.x));
        }
        if(background1.x >=-50){
            movable = false;
        }

       movetroops(movable,player);
       movetroops(movable,enemy);
        if (enemy.fastesttroopPos != null && !enemy.fastesttroopPos.alive){
            enemy.troopsOfPlayer.remove(enemy.fastesttroopPos);
            getFrontPosition(enemy,true);
            kills++;
        }
        if (player.fastesttroopPos != null && !player.fastesttroopPos.alive){
            player.troopsOfPlayer.remove(player.fastesttroopPos);
            getFrontPosition(player,false);
        }

        towerattack(playerTower);
        towerattack(enemyTower);

        //difference = 0;
    }

    public void attackPlayer(Troops attacker){


            if (enemy.fastesttroopPos == null){

                attacker.attack(enemyTower,enemy.troopsOfPlayer, true);
                System.out.println("Check for enemy tower attack x: " +enemyTower.x);
            }
            else {

                attacker.attack(enemy.fastesttroopPos, enemy.troopsOfPlayer, true);


            }
    }



    public void attackEnemy(Troops attacker){

        if (player.fastesttroopPos == null){

                attacker.attack(playerTower, player.troopsOfPlayer, false);

        }
        else{

            attacker.attack(player.fastesttroopPos, player.troopsOfPlayer,false);

        }
    }


    public void movetroops(boolean movable, Player playerObj){

        for (int i = 0; i < playerObj.troopsOfPlayer.size(); i++){
            playerObj.troopsOfPlayer.get(i).move = true;

            if(movable){
                if(moveRight) {
                    playerObj.troopsOfPlayer.get(i).x -= 50;
                }
                else if(moveLeft){
                    playerObj.troopsOfPlayer.get(i).x += 50;
                }
            }
            if(playerObj.name == player.name){
                attackPlayer(playerObj.troopsOfPlayer.get(i));
            }
            else{
                attackEnemy(playerObj.troopsOfPlayer.get(i));
            }

            if (playerObj.troopsOfPlayer.get(i).move){
                if (playerObj.troopsOfPlayer.get(i).updatetrooppicture) {
                    playerObj.troopsOfPlayer.get(i).counter++;
                }
                playerObj.troopsOfPlayer.get(i).updatetrooppicture = !playerObj.troopsOfPlayer.get(i).updatetrooppicture;

                if (playerObj.troopsOfPlayer.get(i).counter >= playerObj.troopsOfPlayer.get(i).picturecounter) {
                    playerObj.troopsOfPlayer.get(i).counter = 0;
                }
                playerObj.troopsOfPlayer.get(i).picture = playerObj.troopsOfPlayer.get(i).pictu.get(playerObj.troopsOfPlayer.get(i).counter);
                playerObj.troopsOfPlayer.get(i).move();
            }

        }

        playerTower.updatePicture();
        enemyTower.updatePicture();
    }


    private void sleep() {
        try {
            Thread.sleep(18);
        }
        catch (Exception e) {

        }
    }

    private void draw(){
        int spSize = 17;
        float scaledSizeInPixels = spSize * getResources().getDisplayMetrics().scaledDensity;
        Paint paint2 = new Paint();
        paint2.setColor(Color.WHITE);
        paint2.setColor(Color.BLACK);
        paint2.setTextSize(scaledSizeInPixels);

        Paint paintattack = new Paint();

        paintattack.setStrokeWidth(14f);

        if (getHolder().getSurface().isValid()){
            Canvas canvas = getHolder().lockCanvas();
            canvas.drawBitmap(background1.background, background1.x,background1.y, paint);
            enemyTower.drawBuilding(canvas,paint);
            playerTower.drawBuilding(canvas,paint);
            if (!playerTower.alive){
                playerTower.health = 0;
            }
            if (!enemyTower.alive){
                enemyTower.health = 0;
            }
            canvas.drawText("HP:" + playerTower.health, playerTower.x + 50, playerTower.y - 75, paint2);
            canvas.drawText("HP:" + enemyTower.health, enemyTower.x + enemyTower.picture.getWidth()/2, enemyTower.y - 75, paint2);


          playerTower.drawAttack(canvas,paintattack,enemy.fastesttroopPos,enemy.troopsOfPlayer);
          enemyTower.drawAttack(canvas,paintattack,player.fastesttroopPos,player.troopsOfPlayer);

          player.drawTroops(canvas,paint, paint2);
          enemy.drawTroops(canvas,paint, paint2);

            canvas.drawText("ECTS:" + player.ECTs,screenX-screenX/9 + 10,screenY/14,paint2);canvas.drawText("Kills:" + kills,screenX-screenX/9 + 10,screenY/14 * 2,paint2);
            getHolder().unlockCanvasAndPost(canvas);
        }

    }




    public void towerattack(Tower a_tower){
        if (a_tower.player == true){
            if (enemy.fastesttroopPos != null){
                a_tower.attack(enemy.fastesttroopPos, enemy.troopsOfPlayer, true);

            }
        }
        else {
            if (player.fastesttroopPos != null ){
                a_tower.attack(player.fastesttroopPos, player.troopsOfPlayer, false);

            }
        }
    }

    public void getFrontPosition(Player playerObj, boolean isEnemy){
        if (playerObj.troopsOfPlayer.isEmpty()){
            if (!isEnemy){
                player.fastesttroopPos = null;
            }
            else{
                enemy.fastesttroopPos = null;
            }
        }
        else{
            if (!isEnemy) {
                Troops test = player.troopsOfPlayer.get(0);
                for (Troops troop:player.troopsOfPlayer) {
                    if (troop.x > test.x){
                        test = troop;
                    }
                }
                player.fastesttroopPos = test;

            }

            else{
                Troops test = enemy.troopsOfPlayer.get(0);
                for (Troops troop:enemy.troopsOfPlayer) {
                    if (troop.x < test.x){
                        test = troop;
                    }
                }
                enemy.fastesttroopPos = test;
            }

        }
    }
    //not working
    void drawFinishScreen(){
        sharedPreferences = conti.getSharedPreferences("Statistics", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        String GamesPlayed = sharedPreferences.getString("GamesPlayed", "0");
        String GamesWon = sharedPreferences.getString("GamesWon", "0");
        GamesPlayed = String.valueOf(Integer.parseInt(GamesPlayed)+1);
        editor.putString("Kills",""+ kills);
        editor.putString("GamesPlayed", GamesPlayed);
        editor.commit();
        playerTower.updatePicture();
        enemyTower.updatePicture();
        draw();

        Paint paint2 = new Paint();
        paint2.setTextSize(100f);
        if (getHolder().getSurface().isValid()) {
            Canvas canvas = getHolder().lockCanvas();
            if(playerTower.health <= 0){
                paint2.setColor(Color.RED);
                canvas.drawText("YOU LOST",screenX/2,screenY/2, paint2);
            }
            else{
                paint2.setColor(Color.GREEN);
                canvas.drawText("YOU WON",screenX/2,screenY/2, paint2);
                GamesWon = String.valueOf(Integer.parseInt(GamesWon)+1);
                editor.putString("GamesWon", GamesWon);
            }

            getHolder().unlockCanvasAndPost(canvas);
        }
        editor.commit();
    }


}



