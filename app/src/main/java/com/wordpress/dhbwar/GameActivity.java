package com.wordpress.dhbwar;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.*;
import android.os.CountDownTimer;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.autofill.AutofillValue;
import android.widget.*;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import io.cucumber.messages.internal.com.google.protobuf.DescriptorProtos;
import org.w3c.dom.Text;

import java.util.Timer;
import java.util.TimerTask;

public class GameActivity extends AppCompatActivity {

    public GameView gameView;
    private float oldx = 0;
    private float result = 0;
    private float oldresult = 0;
    public static SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;



    public ImageButton troop1btn;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View decorView = getWindow().getDecorView();
        // Hide both the navigation bar and the status bar.
        // SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
        // a general rule, you should design your app to hide the status bar whenever you
        // hide the navigation bar.
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

        sharedPreferences = getSharedPreferences("Statistics",Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //region creation of buttons etc. on the gameview
        Point point = new Point();
        getWindowManager().getDefaultDisplay().getSize(point);
        final FrameLayout game = new FrameLayout(this);

        GridLayout gameWidgets = new GridLayout (this);

        gameWidgets.setRowCount(2);
        gameWidgets.setColumnCount(6);
        gameView = new GameView(this,point.x,point.y);

        troop1btn = new ImageButton(this);
        ImageButton troop2_Wizardbtn = new ImageButton(this);
        ImageButton troop3_Giantbtn = new ImageButton(this);

        final Button towerModebtn = new Button(this);
        towerModebtn.setText("SingleMode");
        ImageButton troop4_Supermanbtn = new ImageButton(this);
        troop1btn.setImageBitmap(createMap(R.drawable.stickmanselector));
        troop2_Wizardbtn.setImageBitmap(createMap(R.drawable.wizardselector));
        troop3_Giantbtn.setImageBitmap(createMap(R.drawable.giantselector));
        final Button towerupgradeButton = new Button(this);
        towerupgradeButton.setText("Tower:" + Tower.upgradeCost);




        final Button switchUpgradeModebtn = new Button(this);
        switchUpgradeModebtn.setText("Upgrade");
        int spSize = 4;
        float scaledSizeInPixels = spSize * getResources().getDisplayMetrics().scaledDensity;
        towerupgradeButton.setTextSize(scaledSizeInPixels);
        towerModebtn.setTextSize(scaledSizeInPixels);
        switchUpgradeModebtn.setTextSize(scaledSizeInPixels);


        final TextView troop1_text= new TextView(this);

        troop1_text.setText("" + Troop1.ects_value);
        final TextView troop2_text= new TextView(this);
        troop2_text.setText("" + Troop2_Wizard.ects_value);
        final TextView troop3_text= new TextView(this);
        troop3_text.setText("" + Troop3_Giant.ects_value);
        final TextView troop4_text= new TextView(this);
        troop4_text.setText("" + Troop4_Superman.ects_value);
        troop1_text.setTextColor(Color.RED);
        troop2_text.setTextColor(Color.RED);
        troop3_text.setTextColor(Color.RED);
        troop4_text.setTextColor(Color.RED);
        troop4_Supermanbtn.setImageBitmap(createMap(R.drawable.supermanselector));
        gameWidgets.addView(troop1btn,0);

        gameWidgets.addView(troop2_Wizardbtn,1);
        gameWidgets.addView(troop3_Giantbtn,2);
        gameWidgets.addView(troop4_Supermanbtn,3);
        gameWidgets.addView(switchUpgradeModebtn,4);
        gameWidgets.addView(towerModebtn,5 );
        gameWidgets.addView(troop1_text,6);
        gameWidgets.addView(troop2_text,7);
        gameWidgets.addView(troop3_text,8);
        gameWidgets.addView(troop4_text,9);
        gameWidgets.addView(towerupgradeButton,10);
        towerupgradeButton.setVisibility(View.INVISIBLE);



        //endregion



switchUpgradeModebtn.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        if (switchUpgradeModebtn.getText() == "Troops"){
            switchUpgradeModebtn.setText("Upgrade");
            towerupgradeButton.setVisibility(View.INVISIBLE);
            troop1_text.setText("" +Troop1.ects_value);
            troop2_text.setText("" +Troop2_Wizard.ects_value);
            troop3_text.setText("" +Troop3_Giant.ects_value);
            troop4_text.setText("" +Troop4_Superman.ects_value);
        }
        else  {
            switchUpgradeModebtn.setText("Troops");
            towerupgradeButton.setVisibility(View.VISIBLE);
            troop1_text.setText("" +Troop1.upgradeCost);
            troop2_text.setText("" +Troop2_Wizard.upgradeCost);
            troop3_text.setText("" +Troop3_Giant.upgradeCost);
            troop4_text.setText("" +Troop4_Superman.upgradeCost);
        }
    }
});

        //region Set buttons to select troops
        troop1btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (switchUpgradeModebtn.getText() == "Troops") {
                   gameView.player.upgradeTroop( "Troop1", Troop1.upgradeCost);
                    troop1_text.setText("" + Troop1.upgradeCost);
                }
                else{
                    gameView.player.buyTroop("Troop1", Troop1.ects_value, gameView);
                }
            }
        });

        troop2_Wizardbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (switchUpgradeModebtn.getText() == "Troops") {
                    gameView.player.upgradeTroop( "Troop2_Wizard", Troop2_Wizard.upgradeCost);
                    troop2_text.setText("" + Troop2_Wizard.upgradeCost);
                }
                else{
                    gameView.player.buyTroop("Troop2_Wizard", Troop2_Wizard.ects_value, gameView);
                }

            }
        });


        troop3_Giantbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (switchUpgradeModebtn.getText() == "Troops") {
                    gameView.player.upgradeTroop( "Troop3_Giant", Troop3_Giant.upgradeCost);
                    troop3_text.setText("" + Troop3_Giant.upgradeCost);
                }
                else{
                    gameView.player.buyTroop("Troop3_Giant", Troop3_Giant.ects_value, gameView);
                }

            }
        });

        troop4_Supermanbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (switchUpgradeModebtn.getText() == "Troops") {
                    gameView.player.upgradeTroop( "Troop4_Superman", Troop4_Superman.upgradeCost);
                    troop4_text.setText("" + Troop4_Superman.upgradeCost);
                }
                else{
                    gameView.player.buyTroop("Troop4_Superman", Troop4_Superman.ects_value, gameView);

                }

            }
        });
        //endregion


        towerModebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(towerModebtn.getText() == "SingleMode"){
                    towerModebtn.setText("MultiMode");
                }
                else{
                    towerModebtn.setText("SingleMode");
                }
            gameView.playerTower.switchMode();
            }
        });

        towerupgradeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (gameView.player.ECTs - Tower.upgradeCost >= 0) {
                    gameView.player.ECTs -= Tower.upgradeCost;
                    gameView.playerTower.upgrade();
                    towerupgradeButton.setText("Tower:" + Tower.upgradeCost);

                }
            }
        });

        //region add stuff to gameview
        game.addView(gameView);
        game.addView(gameWidgets);
        setContentView(game);
        //endregion


        //region Gameview listeners for the scrolling
        gameView.setOnClickListener(new View.OnClickListener() {
            @Override
            public  void onClick(View v){

            }
        });

        gameView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {


                if (event.getAction() == MotionEvent.ACTION_DOWN){

                    //oldx = event.getX();

                    System.out.println("X:"  + event.getX() + " y:" + event.getY() );
                    float x = event.getX();

                    if(x< (gameView.screenX/2)){
                        gameView.moveLeft = true;
                        gameView.moveRight = false;
                    }
                    else{
                        gameView.moveRight = true;
                        gameView.moveLeft = false;
                    }

                }
                if (event.getAction() == MotionEvent.ACTION_MOVE) {



                    /*result = (oldx-x) * (-1);
                    gameView.difference = (oldresult - result) * (-1);
                    oldresult = result;
                    System.out.println("difference: "+gameView.difference);*/


                    //return true;
                }
                if(event.getAction() == MotionEvent.ACTION_UP){
                    gameView.moveLeft = false;
                    gameView.moveRight = false;
                }
                return false;
            }
        });
        //endregion


        }



    public Bitmap createMap(int resource){
        Bitmap temp = BitmapFactory.decodeResource(gameView.conti.getResources(), resource);
        temp = Bitmap.createScaledBitmap(temp,gameView.screenX / 8,gameView.screenY/8,true);
        return temp;
    }

    @Override
    protected void onPause() {
        super.onPause();
        gameView.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        gameView.resume();
    }
}