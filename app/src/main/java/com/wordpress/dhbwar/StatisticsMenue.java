package com.wordpress.dhbwar;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import  java.util.*;
import  java.io.*;

public class StatisticsMenue extends AppCompatActivity {

    String KillsAllTime = "0";
    String Kills = "0";
    String ects = "0";
    String GamesPlayed = "0";
    String GamesWon = "0";
    public static SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.statistics_menue);
        Button GoBackBtn = (Button)findViewById(R.id.buttonGoBackStatis);
        GoBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openactivity1();
            }
        });

        sharedPreferences = getSharedPreferences("Statistics", Context.MODE_PRIVATE);
        final TextView statis = findViewById(R.id.textViewtest);

        KillsAllTime = sharedPreferences.getString("KillsAlltime", "0");
        Kills  = sharedPreferences.getString("Kills", "0");
        ects = sharedPreferences.getString("ects", "0");
        GamesPlayed = sharedPreferences.getString("GamesPlayed", "0");
        GamesWon = sharedPreferences.getString("GamesWon", "0");
        statis.setText("Alltime Kills: "+ KillsAllTime +"\n"+
                "Kills last Game: "+ Kills + "\n" +
                "ECTS earned: " + ects + "\n" +
                "Games Played: " + GamesPlayed + "\n" +
                "Games Won: " + GamesWon);
    }
    public void openactivity1(){
        Intent intent = new Intent(this, StartScreen.class);
        startActivity(intent);
    }











    }
