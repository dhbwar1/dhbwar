package com.wordpress.dhbwar;

import android.renderscript.ScriptIntrinsicYuvToRGB;

import java.util.List;

public class SingleTargetBehavior implements AttackBehavior{
    int damage;
    int speed;
    int range;

    public  SingleTargetBehavior(int dmg,int spd, int rg){
        damage = dmg;
        speed = spd;
        range = rg;

    }
    public boolean fight(Troops frontTroop, List<Troops> attackedTroops, boolean enemy,int x, boolean move){
        if(enemy) {
            if (x + speed + range >= frontTroop.x-20) {
                doDamge(frontTroop);
                return false;
            }
        }
        else{
            if(x + speed - range <= frontTroop.x +20){
                doDamge(frontTroop);
                return false;
            }
        }
        return true;

    }
    public void doDamge(Troops frontTroop){
        frontTroop.health -= damage;
        System.out.println("Friendly Troop1 ist attacking");
        if (frontTroop.health <= 0) {
            frontTroop.alive = false;
        }
    }
}
