package com.wordpress.dhbwar;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;


public class StartScreen extends AppCompatActivity {
    public static final String EXTRA_TEXT = "com.wordpress.dhbwar.EXTRA_TEXT";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start_screen);
        final Button StartGameBtn;
        Button StatisticsBtn;
        Button SettingsBtn;
        final Button GoBackBtn;

        final TextView UsernameTxt;
        final String[] Username = new String[1];
        StartGameBtn = (Button)findViewById(R.id.button2);
        StatisticsBtn = (Button)findViewById(R.id.button);
        SettingsBtn = (Button)findViewById(R.id.button3);
        UsernameTxt = (TextView)findViewById(R.id.editTextTextPersonName);




        StartGameBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                    openActivity2();



            }
        });

        StatisticsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivity3();
            }
        });











        /*SettingsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setContentView(R.layout.settings);
            }
        });*/

    }

    public void openActivity2() {
        TextView UsernameTxt = findViewById(R.id.editTextTextPersonName);
        if (UsernameTxt.length() > 0){
        Intent intent = new Intent(this, SelectGameMode.class);
        intent.putExtra(EXTRA_TEXT,UsernameTxt.getText().toString());
        startActivity(intent);}
        else{
            Toast.makeText(this, "Please enter a Username!",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void openActivity3() {

        Intent intent = new Intent(this, StatisticsMenue.class);

        startActivity(intent);
    }
}