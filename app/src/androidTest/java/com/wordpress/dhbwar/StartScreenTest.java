package com.wordpress.dhbwar;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import androidx.test.espresso.Espresso;
import androidx.test.rule.ActivityTestRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static androidx.core.content.ContextCompat.getSystemService;
import static androidx.test.espresso.Espresso.closeSoftKeyboard;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.junit.Assert.*;

public class StartScreenTest {

    @Rule
    public ActivityTestRule<StartScreen> myGameActivty = new ActivityTestRule<StartScreen>(StartScreen.class);




    Instrumentation.ActivityMonitor monitor = getInstrumentation().addMonitor(StatisticsMenue.class.getName(),null,false);
    Instrumentation.ActivityMonitor monitor2 = getInstrumentation().addMonitor(SelectGameMode.class.getName(),null,false);


    @Before
    public  void setup() throws Exception{

    }


    @Test
    public void checkforUserwhenStartedgame(){

        StartScreen mActivity;
        mActivity = myGameActivty.getActivity();


        assertNotNull(mActivity.findViewById(R.id.button2));

        onView(withId(R.id.editTextTextPersonName)).perform(click());

        assertNotNull(mActivity.findViewById(R.id.editTextTextPersonName));
        TextView text = mActivity.findViewById(R.id.editTextTextPersonName);
        text.setText("Test");
        closeSoftKeyboard();


        onView(withId(R.id.button2)).perform(click());
        Activity selectGameMode = getInstrumentation().waitForMonitorWithTimeout(monitor2,5000);

        assertNotNull(selectGameMode);

    }

    @Test
    public void testStartofStats(){
        StartScreen mActivity;
        mActivity = myGameActivty.getActivity();
        assertNotNull(mActivity.findViewById(R.id.button));

        onView(withId(R.id.button)).perform(click());
        Activity statActivity = getInstrumentation().waitForMonitorWithTimeout(monitor,5000);
        assertNotNull(statActivity);
    }





}
