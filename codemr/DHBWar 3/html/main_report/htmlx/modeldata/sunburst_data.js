function EQ_GET_DATA(){ 
	  var ret = {
"name": " DHBWar", "value":876, 
"prmetrics":{"5":1,"6":1,"7":1,"8":1,"9":4,"10":1,"11":3},
"prmetricvalues":{"5":0,"6":18,"7":1,"8":876,"9":40,"10":1,"11":223},
"children": [ {
"name": "com.wordpress.dhbwar", "key": "j", "value":876, 
"pmetrics":{"4":3,"12":3,"13":1,"14":3,"3":1,"1":1,"0":3,"6":3,"8":2,"2":3,"15":1,"16":1},
"pmetricvalues":{"4":3,"12":15,"13":1,"14":17,"3":1,"1":1,"0":3,"17":0.111,"6":18,"18":0.111,"8":876,"19":1.0,"2":3,"15":0,"16":167},
"children":[
{
"name": "ScreenBeforeGame","key": "q","value":12, 
"metrics":{"20":1,"21":1,"22":1,"23":1,"24":1,"25":1,"26":3,"0":3,"27":1,"28":1,"29":1,"30":1,"31":1,"32":1,"33":1,"34":1,"35":1,"36":1,"16":1,"37":1,"4":1,"8":1,"2":1,"3":1,"1":3},
"metricvalues":{"20":4,"21":0,"22":0,"23":0.0,"24":11,"25":0,"26":9,"0":3,"27":1,"28":5,"29":0.0,"30":0.0,"31":0,"32":0.0,"33":0,"34":4,"35":0,"36":4,"16":1,"37":0,"4":1,"8":12,"2":1,"3":1,"1":3}
},
{
"name": "MultiTargetBehavior","key": "o","value":32, 
"metrics":{"20":1,"21":1,"22":1,"23":1,"24":1,"25":1,"26":1,"0":1,"27":1,"28":1,"29":1,"30":2,"31":1,"32":1,"33":1,"34":1,"35":1,"36":1,"16":1,"37":1,"4":1,"8":1,"2":1,"3":1,"1":1},
"metricvalues":{"20":1,"21":0,"38":1,"22":3,"23":0.0,"24":28,"25":0,"26":1,"0":1,"27":3,"28":5,"29":0.333,"30":0.667,"31":0,"32":0.4,"39":1,"33":1,"34":3,"40":0,"35":0,"36":0,"16":13,"37":0,"4":1,"8":32,"2":1,"3":1,"1":1}
},
{
"name": "Troop2_Wizard","key": "x","value":34, 
"metrics":{"20":1,"21":1,"22":1,"23":1,"24":1,"25":1,"26":2,"0":2,"27":1,"28":1,"29":1,"30":3,"31":1,"32":1,"33":1,"34":1,"35":1,"36":1,"16":1,"37":1,"4":1,"8":1,"2":1,"3":1,"1":2},
"metricvalues":{"20":4,"21":0,"38":1,"22":2,"23":0.0,"24":24,"25":0,"26":2,"0":2,"27":1,"28":12,"29":0.0,"30":0.778,"31":7,"32":0.25,"39":1,"33":1,"34":1,"40":0,"35":1,"36":3,"16":5,"37":0,"4":1,"8":34,"2":1,"3":1,"1":2}
},
{
"name": "StatisticsMenue","key": "u","value":32, 
"metrics":{"20":2,"21":1,"22":1,"23":1,"24":1,"25":1,"26":3,"0":3,"27":1,"28":1,"29":1,"30":1,"31":1,"32":1,"33":1,"34":1,"35":1,"36":2,"16":1,"37":1,"4":1,"8":1,"2":2,"3":1,"1":3},
"metricvalues":{"20":9,"21":1,"22":5,"23":0.0,"24":25,"25":0,"26":9,"0":3,"27":2,"28":10,"29":0.0,"30":0.0,"31":1,"32":0.25,"33":0,"34":8,"35":0,"36":9,"16":2,"37":0,"4":1,"8":32,"2":2,"3":1,"1":3}
},
{
"name": "Tower","key": "v","value":75, 
"metrics":{"20":2,"21":1,"22":1,"23":3,"24":2,"25":1,"26":2,"0":2,"27":1,"28":1,"29":1,"30":4,"31":1,"32":2,"33":1,"34":1,"35":1,"36":2,"16":1,"37":1,"4":2,"8":2,"2":2,"3":2,"1":2},
"metricvalues":{"20":10,"21":0,"38":1,"22":4,"23":0.333,"24":68,"25":0,"26":2,"0":2,"27":6,"28":18,"29":0.2,"30":0.867,"31":2,"32":0.646,"39":1,"33":2,"34":13,"40":0,"35":0,"36":8,"16":20,"37":1,"4":2,"8":75,"2":2,"3":2,"1":2}
},
{
"name": "Background","key": "l","value":8, 
"metrics":{"20":1,"21":1,"22":1,"23":1,"24":1,"25":1,"26":1,"0":1,"27":1,"28":1,"29":1,"30":1,"31":1,"32":1,"33":1,"34":1,"35":1,"36":1,"16":1,"37":1,"4":1,"8":1,"2":1,"3":1,"1":1},
"metricvalues":{"20":4,"21":0,"38":1,"22":3,"23":0.0,"24":5,"25":0,"26":1,"0":1,"27":1,"28":4,"29":0.0,"30":0.0,"31":0,"32":0.0,"39":0,"33":0,"34":3,"40":1,"35":0,"36":4,"16":2,"37":0,"4":1,"8":8,"2":1,"3":1,"1":1}
},
{
"name": "GameActivity","key": "m","value":160, 
"metrics":{"20":4,"21":1,"22":1,"23":1,"24":2,"25":1,"26":3,"0":4,"27":1,"28":1,"29":1,"30":4,"31":1,"32":1,"33":1,"34":1,"35":1,"36":4,"16":1,"37":1,"4":2,"8":2,"2":4,"3":1,"1":3},
"metricvalues":{"20":26,"21":1,"38":1,"22":6,"23":0.0,"24":152,"25":0,"26":9,"0":4,"27":4,"28":40,"29":0.0,"30":1.0,"31":1,"32":0.5,"39":1,"33":5,"34":35,"40":0,"35":0,"36":21,"16":4,"37":0,"4":2,"8":160,"2":4,"3":1,"1":3}
},
{
"name": "Troop3_Giant","key": "y","value":30, 
"metrics":{"20":1,"21":1,"22":1,"23":1,"24":1,"25":1,"26":2,"0":2,"27":1,"28":1,"29":1,"30":4,"31":1,"32":1,"33":1,"34":1,"35":1,"36":1,"16":1,"37":1,"4":1,"8":1,"2":1,"3":1,"1":2},
"metricvalues":{"20":4,"21":0,"38":1,"22":2,"23":0.0,"24":22,"25":0,"26":2,"0":2,"27":1,"28":12,"29":0.0,"30":0.857,"31":5,"32":0.25,"39":1,"33":1,"34":1,"40":0,"35":1,"36":3,"16":5,"37":0,"4":1,"8":30,"2":1,"3":1,"1":2}
},
{
"name": "AttackBehavior","key": "k","value":2, 
"metrics":{"20":1,"21":1,"22":1,"23":1,"24":1,"25":2,"26":1,"0":1,"27":1,"28":1,"29":1,"30":1,"31":1,"32":1,"33":1,"34":1,"35":1,"36":1,"37":1,"4":1,"8":1,"2":1,"3":1,"16":1,"1":1},
"metricvalues":{"20":1,"21":0,"38":2,"22":0,"23":0.0,"24":1,"25":2,"26":1,"0":1,"27":1,"28":1,"29":0.0,"30":0.0,"31":0,"32":0.0,"39":0,"33":1,"34":0,"40":2,"35":0,"36":0,"37":0,"4":1,"8":2,"2":1,"3":1,"16":1,"1":1}
},
{
"name": "Troop4_Superman","key": "z","value":35, 
"metrics":{"20":1,"21":1,"22":1,"23":1,"24":1,"25":1,"26":2,"0":2,"27":1,"28":1,"29":1,"30":2,"31":1,"32":1,"33":1,"34":1,"35":1,"36":1,"16":1,"37":1,"4":1,"8":1,"2":1,"3":1,"1":2},
"metricvalues":{"20":4,"21":0,"38":1,"22":2,"23":0.0,"24":25,"25":0,"26":2,"0":2,"27":1,"28":12,"29":0.0,"30":0.667,"31":7,"32":0.25,"39":1,"33":1,"34":1,"40":0,"35":1,"36":3,"16":5,"37":0,"4":1,"8":35,"2":1,"3":1,"1":2}
},
{
"name": "GameView","key": "n","value":230, 
"metrics":{"20":3,"21":1,"22":2,"23":1,"24":2,"25":1,"26":2,"0":3,"27":1,"28":2,"29":1,"30":4,"31":1,"32":3,"33":2,"34":2,"35":1,"36":3,"16":3,"37":1,"4":2,"8":2,"2":3,"3":3,"1":3},
"metricvalues":{"20":20,"21":0,"38":11,"22":27,"23":0.0,"24":203,"25":0,"26":3,"0":3,"27":13,"28":61,"29":0.361,"30":0.893,"31":1,"32":0.758,"39":9,"33":9,"34":54,"40":2,"35":0,"36":11,"16":65,"37":0,"4":2,"8":230,"2":3,"3":3,"1":3}
},
{
"name": "Player","key": "p","value":55, 
"metrics":{"20":3,"21":1,"22":1,"23":1,"24":1,"25":1,"26":1,"0":3,"27":1,"28":1,"29":1,"30":4,"31":1,"32":1,"33":2,"34":1,"35":1,"36":1,"16":1,"37":1,"4":2,"8":2,"2":3,"3":1,"1":1},
"metricvalues":{"20":11,"21":0,"38":7,"22":7,"23":0.0,"24":47,"25":0,"26":1,"0":3,"27":4,"28":16,"29":0.333,"30":1.0,"31":0,"32":0.458,"39":6,"33":7,"34":12,"40":1,"35":0,"36":4,"16":16,"37":0,"4":2,"8":55,"2":3,"3":1,"1":1}
},
{
"name": "StartScreen","key": "t","value":38, 
"metrics":{"20":2,"21":1,"22":1,"23":1,"24":1,"25":1,"26":3,"0":3,"27":1,"28":1,"29":5,"30":1,"31":1,"32":1,"33":1,"34":1,"35":1,"36":2,"16":1,"37":1,"4":1,"8":1,"2":2,"3":1,"1":3},
"metricvalues":{"20":8,"21":0,"22":0,"23":0.0,"24":36,"25":0,"26":9,"0":3,"27":3,"28":14,"29":1.0,"30":0.0,"31":1,"32":0.333,"33":0,"34":11,"35":0,"36":8,"16":4,"37":0,"4":1,"8":38,"2":2,"3":1,"1":3}
},
{
"name": "Troop1","key": "w","value":29, 
"metrics":{"20":1,"21":1,"22":1,"23":1,"24":1,"25":1,"26":2,"0":2,"27":1,"28":1,"29":1,"30":4,"31":1,"32":1,"33":1,"34":1,"35":1,"36":1,"16":1,"37":1,"4":1,"8":1,"2":1,"3":1,"1":2},
"metricvalues":{"20":4,"21":0,"38":1,"22":2,"23":0.0,"24":21,"25":0,"26":2,"0":2,"27":1,"28":12,"29":0.0,"30":0.857,"31":5,"32":0.25,"39":1,"33":1,"34":1,"40":0,"35":1,"36":3,"16":5,"37":0,"4":1,"8":29,"2":1,"3":1,"1":2}
},
{
"name": "Troops","key": "A","value":45, 
"metrics":{"20":1,"21":1,"22":1,"23":1,"24":1,"25":2,"26":1,"0":1,"27":1,"28":1,"29":2,"30":5,"31":1,"32":1,"33":1,"34":1,"35":1,"36":1,"16":1,"37":1,"4":1,"8":1,"2":1,"3":1,"1":1},
"metricvalues":{"20":5,"21":0,"38":5,"22":19,"23":0.0,"24":25,"25":5,"26":1,"0":1,"27":4,"28":14,"29":0.667,"30":1.05,"31":1,"32":0.5,"39":0,"33":1,"34":10,"40":5,"35":0,"36":4,"16":7,"37":0,"4":1,"8":45,"2":1,"3":1,"1":1}
},
{
"name": "Troopselector","key": "B","value":8, 
"metrics":{"20":1,"21":1,"22":1,"23":1,"24":1,"25":1,"26":1,"0":1,"27":1,"28":1,"29":1,"30":1,"31":1,"32":1,"33":1,"34":1,"35":1,"36":1,"16":1,"37":1,"4":1,"8":1,"2":1,"3":1,"1":1},
"metricvalues":{"20":4,"21":0,"38":1,"22":3,"23":0.0,"24":5,"25":0,"26":1,"0":1,"27":1,"28":4,"29":0.0,"30":0.0,"31":0,"32":0.0,"39":0,"33":0,"34":3,"40":1,"35":0,"36":4,"16":2,"37":0,"4":1,"8":8,"2":1,"3":1,"1":1}
},
{
"name": "SingleTargetBehavior","key": "s","value":23, 
"metrics":{"20":1,"21":1,"22":1,"23":1,"24":1,"25":1,"26":1,"0":1,"27":1,"28":1,"29":1,"30":1,"31":1,"32":1,"33":1,"34":1,"35":1,"36":1,"16":1,"37":1,"4":1,"8":1,"2":1,"3":1,"1":1},
"metricvalues":{"20":1,"21":0,"38":1,"22":3,"23":0.0,"24":19,"25":0,"26":1,"0":1,"27":3,"28":4,"29":0.333,"30":0.5,"31":0,"32":0.4,"39":1,"33":1,"34":2,"40":0,"35":0,"36":0,"16":7,"37":0,"4":1,"8":23,"2":1,"3":1,"1":1}
},
{
"name": "SelectGameMode","key": "r","value":28, 
"metrics":{"20":2,"21":1,"22":1,"23":1,"24":1,"25":1,"26":3,"0":3,"27":1,"28":1,"29":1,"30":1,"31":1,"32":1,"33":1,"34":1,"35":1,"36":2,"16":1,"37":1,"4":1,"8":1,"2":2,"3":1,"1":3},
"metricvalues":{"20":8,"21":0,"22":0,"23":0.0,"24":27,"25":0,"26":9,"0":3,"27":3,"28":11,"29":0.0,"30":0.0,"31":0,"32":0.333,"33":1,"34":8,"35":0,"36":7,"16":3,"37":0,"4":1,"8":28,"2":2,"3":1,"1":3}
}
]
}]
 }
;
return ret;
}
var EQ_METRIC_MAP = {};
EQ_METRIC_MAP["C3"] =0;
EQ_METRIC_MAP["Complexity"] =1;
EQ_METRIC_MAP["Coupling"] =2;
EQ_METRIC_MAP["Lack of Cohesion"] =3;
EQ_METRIC_MAP["Size"] =4;
EQ_METRIC_MAP["Number of Highly Problematic Classes"] =5;
EQ_METRIC_MAP["Number of Entities"] =6;
EQ_METRIC_MAP["Number of Problematic Classes"] =7;
EQ_METRIC_MAP["Class Lines of Code"] =8;
EQ_METRIC_MAP["Number of External Packages"] =9;
EQ_METRIC_MAP["Number of Packages"] =10;
EQ_METRIC_MAP["Number of External Entities"] =11;
EQ_METRIC_MAP["Efferent Coupling"] =12;
EQ_METRIC_MAP["Number of Interfaces"] =13;
EQ_METRIC_MAP["Number of Classes"] =14;
EQ_METRIC_MAP["Afferent Coupling"] =15;
EQ_METRIC_MAP["Weighted Method Count"] =16;
EQ_METRIC_MAP["Normalized Distance"] =17;
EQ_METRIC_MAP["Abstractness"] =18;
EQ_METRIC_MAP["Instability"] =19;
EQ_METRIC_MAP["Coupling Between Object Classes"] =20;
EQ_METRIC_MAP["Access to Foreign Data"] =21;
EQ_METRIC_MAP["Number of Fields"] =22;
EQ_METRIC_MAP["Specialization Index"] =23;
EQ_METRIC_MAP["Class-Methods Lines of Code"] =24;
EQ_METRIC_MAP["Number of Children"] =25;
EQ_METRIC_MAP["Depth of Inheritance Tree"] =26;
EQ_METRIC_MAP["Number of Methods"] =27;
EQ_METRIC_MAP["Response For a Class"] =28;
EQ_METRIC_MAP["Lack of Tight Class Cohesion"] =29;
EQ_METRIC_MAP["Lack of Cohesion of Methods"] =30;
EQ_METRIC_MAP["Number of Static Fields"] =31;
EQ_METRIC_MAP["Lack of Cohesion Among Methods(1-CAM)"] =32;
EQ_METRIC_MAP["CBO App"] =33;
EQ_METRIC_MAP["Simple Response For a Class"] =34;
EQ_METRIC_MAP["Number of Static Methods"] =35;
EQ_METRIC_MAP["CBO Lib"] =36;
EQ_METRIC_MAP["Number of Overridden Methods"] =37;
EQ_METRIC_MAP["Degree"] =38;
EQ_METRIC_MAP["OutDegree"] =39;
EQ_METRIC_MAP["InDegree"] =40;
var EQ_SELECTED_CLASS_METRIC 		= "Coupling";
var EQ_SELECTED_PACKAGE_METRIC 	= "Coupling";
var EQ_SELECTED_PROJECT_METRIC 	= "Class Lines of Code";
var EQ_CLASS_METRIC_INDEX 	= EQ_METRIC_MAP[EQ_SELECTED_CLASS_METRIC];
var EQ_PACKAGE_METRIC_INDEX	= EQ_METRIC_MAP[EQ_SELECTED_PACKAGE_METRIC];
var EQ_PROJECT_METRIC_INDEX 	= EQ_METRIC_MAP[EQ_SELECTED_PROJECT_METRIC];
var EQ_COLOR_OF_LEVELS = ["#1F77B4","#007F24","#62BF18","#FFC800","#FF5B13","#E50000"];
var EQ_CLASS_METRICS = ["C3","Complexity","Coupling","Lack of Cohesion","Size","Class Lines of Code","Weighted Method Count","Coupling Between Object Classes","Access to Foreign Data","Number of Fields","Specialization Index","Class-Methods Lines of Code","Number of Children","Depth of Inheritance Tree","Number of Methods","Response For a Class","Lack of Tight Class Cohesion","Lack of Cohesion of Methods","Number of Static Fields","Lack of Cohesion Among Methods(1-CAM)","CBO App","Simple Response For a Class","Number of Static Methods","CBO Lib","Number of Overridden Methods","Degree","OutDegree","InDegree"];
var EQ_PACKAGE_METRICS = ["C3","Complexity","Coupling","Lack of Cohesion","Size","Number of Entities","Class Lines of Code","Efferent Coupling","Number of Interfaces","Number of Classes","Afferent Coupling","Weighted Method Count","Normalized Distance","Abstractness","Instability"];
var EQ_PROJECT_METRICS = ["Number of Highly Problematic Classes","Number of Entities","Number of Problematic Classes","Class Lines of Code","Number of External Packages","Number of Packages","Number of External Entities"];
function EQ_GET_COLOR(d) {
if(d.metrics)
return EQ_COLOR_OF_LEVELS[d.metrics[EQ_CLASS_METRIC_INDEX]];
if(d.pmetrics)
return EQ_COLOR_OF_LEVELS[d.pmetrics[EQ_PACKAGE_METRIC_INDEX]];
if(d.prmetrics)
return EQ_COLOR_OF_LEVELS[d.prmetrics[EQ_PROJECT_METRIC_INDEX]];
return EQ_COLOR_OF_LEVELS[0];
}
